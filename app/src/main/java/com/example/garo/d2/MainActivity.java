package com.example.garo.d2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    /*  1) Изначально в строке 4 одинаковых символа, после каждого превращения будем иметь 4
    одинаковые символьные последовательности длиной 2*N, где N - номер строки. Это позволяет
    предположить, что расчеты для данной последовательности сводятся к расчетам для одного
    символа 1, результаты которых умножаются на 4.
        2) Элемент появляется в последовательности, когда: а) номер строки, в которой он нахо-
    дится, больше самого элемента; б) при наличии в предыдущей строке элементов, меньших его
    на 1. Так как оба утверждения справедливы для каждого элемента, расчет количества появле-
    ний в строке каждого элемента сводится к нахождению элемента треугольника Паскаля, где
    номер его строки будет совпадать с номером строки из начального условия задачи, а номер
    элемента в строке - с числовым значением данного в условии. Из пункта (1) следует, что по-
    лученное значение домножается на число, равное количеству повторений символа 1-й строки,
    в данной задаче это 4.
    */

    public int trng[][] = new int[10][10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Подготовка NumberPicker'ов к вводу исходных данных
        NumberPicker num1 = (NumberPicker) findViewById(R.id.Num1);
        NumberPicker num2 = (NumberPicker) findViewById(R.id.Num2);
        NumberPicker strnum = (NumberPicker) findViewById(R.id.StrNum);
        num1.setMaxValue(9);
        num1.setMinValue(1);
        num1.setValue(5);
        num2.setMaxValue(9);
        num2.setMinValue(1);
        num2.setValue(7);
        strnum.setMaxValue(9);
        strnum.setMinValue(1);
        strnum.setValue(9);

        // Получение треугольника Паскаля, умноженного на 4, в виде двумерного массива
        for(int i=0; i<10; i++){
            trng[i][0]=4;
            trng[i][i]=4;
        }
        for (int i=2; i<10; i++)
            for (int j=1; j<i; j++)
                trng[i][j] = trng[i-1][j-1]+trng[i-1][j];
    }

    public void onclick1(View v){
        // Получение исходных данных задачи
        NumberPicker num1 = (NumberPicker) findViewById(R.id.Num1);
        NumberPicker num2 = (NumberPicker) findViewById(R.id.Num2);
        NumberPicker strnum = (NumberPicker) findViewById(R.id.StrNum);
        int n = strnum.getValue();
        int a = num1.getValue();
        int b = num2.getValue();

        // Выбор требуемого значения и формирование результата решения задачи
        String message = "Элементов в строке под номером "+Integer.toString(n)+":\n";
        message += "\""+Integer.toString(a)+"\": "+Integer.toString(trng[n-1][a-1]);
        if (a!=b) message += "\n\""+Integer.toString(b)+"\": "+Integer.toString(trng[n-1][b-1]);

        // Вывод результата на дисплей
        Toast result = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG);
        result.show();
    }
}
